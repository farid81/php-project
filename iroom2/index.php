<?php
  require_once('includes/functions.php');

  if ($loggedIn == true) {
    header('Location: myroom.php?user=' . $currentUser['user_name']);
  }

  echo $twig->render('landing_page.html');
?>
