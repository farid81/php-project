<?php
  require_once('includes/functions.php');

  if ($loggedIn && $_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!isset($_POST['sender']) || !isset($_POST['recipient']) || !isset($_POST['message'])) {
      outputError(0, $twig);
    }
    else {
      // Sanitize inputs
      $sender = filter_var($_POST['sender'], FILTER_SANITIZE_STRING);
      $recipient = filter_var($_POST['recipient'], FILTER_SANITIZE_STRING);
      $message = prepareInputString($_POST['message'], 200);

      // Check if both users exist and are friends
      $friendTest = DB::queryFirstRow('SELECT * FROM friendship WHERE user_name=%s AND friend_name=%s', $sender, $recipient);

      if ($friendTest == null) {
        outputError(0, $twig);
      }
      else {
        // All is good, add shout
        DB::insert('messages', array(
          'sender' => $sender,
          'recipient' => $recipient,
          'message' => $message,
          'time_sent' => time()
        ));
        header('Location: myroom.php?user=' . $recipient . '#shouts');
      }
    }
  }
  else {
    outputError(0, $twig);
  }
?>
