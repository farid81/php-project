<?php
  require_once('includes/functions.php');

  // Redirect user if already logged in
  if ($loggedIn == true) {
    header(sprintf('Location: myroom.php?user=%s', $currentUser['user_name']));
  }

  // Create arrays to hold all errors and entered values
  $errors = array();
  $values = array();


  // Check if user is submitting registration
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Create array of all users to check if name is taken
    $users = DB::query("SELECT user_name FROM users");
    // Initialize variable to contain sanitized email
    $email = '';
    // Initialize variable to contain display name after sanitizing inappropriate language
    $displayName = '';

    // Validate display name
    if (!isset($_POST['displayName']) || strlen(trim($_POST['displayName'])) < 1 || strlen(trim($_POST['displayName'])) > 30) {
      $errors['errDispName'] = true;
    }
    else {
      $displayName = cleanBadWords(filter_var($_POST['displayName'], FILTER_SANITIZE_STRING));
    }

    // Validate user name
    // Check if username meets length requirements
    if (!isset($_POST['userName']) || strlen(trim($_POST['userName'])) < 3 || strlen(trim($_POST['userName'])) > 16) {
      $errors['errUserName'] = 1;
    }
    else if (preg_match('/\s/', $_POST['userName'])) {
      // Check if there is any whitespace in the user name
      $errors['errUserName'] = 3;
    }
    else {
      // Check if username is already taken
      foreach ($users as $user) {
        if (strtolower($_POST['userName']) == $user['user_name']) {
          $errors['errUserName'] = 2;
          break;
        }
      }
    }

    // Validate email
    if (!isset($_POST['email']) || trim($_POST['email']) == '') {
      $errors['errEmail'] = true;
    }
    else {
      // Sanitize email;
      $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

      // Check if email is invalid
      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors['errEmail'] = true;
      }
    }

    // Validate passwords
    if (!isset($_POST['password']) || trim($_POST['password']) == '') {
      $errors['errPassword'] = true;
    }
    else if (!isset($_POST['passwordRetype']) || trim($_POST['passwordRetype']) == '') {
      $errors['errPasswordRetype'] = 1;
    }
    else {
      // Make sure password meets length requirements
      if (strlen($_POST['password']) < 8 || strlen($_POST['password']) > 16) {
        $errors['errPassword'] = true;
      }
      else {
        // Check if the two passwords match
        if ($_POST['password'] != $_POST['passwordRetype']) {
          $errors['errPasswordRetype'] = 2;
        }
      }
    }

    // If there are no errors, insert new user into the db, and redirect to login
    if (count($errors) == 0) {
      // Create epoch time out of entered birthday for storage
      $bdayEpoch = strtotime(filter_var($_POST['birthday'], FILTER_SANITIZE_STRING));
      if ($bdayEpoch == false) {
        $bdayEpoch = NULL;
      }

      DB::insert('users', array(
        'user_name' => trim(strtolower($_POST['userName'])),
        'registered' => time(),
        'display_name' => trim($displayName),
        'email' => trim($email),
        'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
        'birthday' => $bdayEpoch,
        'friend_key' => createFriendToken(6)
      ));

      header('Location: login.php?newAccount=true');
    }
    else {
      // If there are errors, populate array of entered values to show in the form
      $values['displayName'] = isset($_POST['displayName']) ? $displayName : '';
      $values['userName'] = isset($_POST['userName']) ? strtolower($_POST['userName']) : '';
      $values['email'] = isset($_POST['email']) ? $email : '';
      $values['birthday'] = isset($_POST['birthday']) ? $_POST['birthday'] : '';
    }
  }


  // Render page
  echo $twig->render('register.html', array_merge($errors, $values, array(
    'loggedIn' => $loggedIn,
    'sessDisplayName' => $loggedIn == true ? $currentUser['display_name'] : false
  )));
?>
