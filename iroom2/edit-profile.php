<?php
  require_once('includes/functions.php');

  if ($loggedIn) {
    // Create variable to determine if there was an error with file upload
    $fileError = false;

    // Create array of values to fill the boxes on the page
    $statusValue = $currentUser['status'];
    $foodValue = $currentUser['fave_food'];
    $movieValue = $currentUser['fave_movie'];
    $gameValue = $currentUser['fave_game'];
    $hangoutTimeValue = date('Y - m - d', $currentUser['hangout_time']);

    // Check if user is submitting a change
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Set values to fill boxes on page with the submitted values, in case we're back here due to an error
      $statusValue = $_POST['status'];
      $foodValue = $_POST['food'];
      $movieValue = $_POST['movie'];
      $gameValue = $_POST['game'];
      $hangoutTimeValue = $_POST['hangout_time'];

      // Process input strings for sanitization, profanity, and length
      if ($_POST['status'] != '') {
        $status = prepareInputString($_POST['status'], 150);
      }
      else {
        $status = 'test';
      }

      if ($_POST['food'] != '') {
        $food = prepareInputString($_POST['food'], 50);
      }
      else {
        $food = '';
      }

      if ($_POST['movie'] != '') {
        $movie = prepareInputString($_POST['movie'], 50);
      }
      else {
        $movie = '';
      }

      if ($_POST['game'] != '') {
        $game = prepareInputString($_POST['game'], 50);
      }
      else {
        $game = '';
      }

      if ($_POST['hangout_time'] != '') {
        $hangoutTime = strtotime(filter_var($_POST['hangout_time'], FILTER_SANITIZE_STRING));
      }
      else {
        $hangoutTime = null;
      }

      // Update text status and info
      DB::update('users', array(
        'status' => $status,
        'fave_food' => $food,
        'fave_movie' => $movie,
        'fave_game' => $game,
        'hangout_time' => $hangoutTime
      ), 'user_name=%s', $currentUser['user_name']);


      // Check if a new image was uploaded and process accordingly
      if ($_FILES['img_file']['name'] != '') {
        $path = sprintf('content/user/%s/', $currentUser['user_name']);
        $fileType = strtolower(pathinfo($path . basename($_FILES["img_file"]["name"]), PATHINFO_EXTENSION));
        $fileName = md5($_FILES['img_file']['name']) . '.' . $fileType;

        // Check if file is too large
        if ($_FILES['img_file']['size'] > 10000000) {
          $fileError = true;
        }

        // Check if file is not an image
        if ($fileType != 'jpg' && $fileType != 'jpeg' && $fileType != 'png') {
          $fileError = true;
        }

        // Check if file is not actually an image
        if (getimagesize($_FILES["img_file"]["tmp_name"]) == false) {
          $fileError = true;
        }

        // If file is okay, attempt upload
        if ($fileError == false) {
          if (!file_exists($path)) {
            mkdir($path);
          }

          if (move_uploaded_file($_FILES["img_file"]["tmp_name"], $path . $fileName)) {
            DB::update('users', array(
              'room_image' => $fileName
            ), 'user_name=%s', $currentUser['user_name']);
          }
          else {
            $fileError = true;
          }
        }
      }


      // Redirect to profile page
      if ($fileError == false) {
        header(sprintf('Location: myroom.php?user=%s', $currentUser['user_name']));
      }
    }


    // Render page
    echo $twig->render('login_info_update.html', array(
      'loggedIn' => $loggedIn,
      'sessUserName' => $currentUser['user_name'],
      'sessDisplayName' => $loggedIn == true ? $currentUser['display_name'] : false,
      'current_status' => $statusValue,
      'current_food' => $foodValue,
      'current_movie' => $movieValue,
      'current_game' => $gameValue,
      'current_hangout_time' => $hangoutTimeValue == null ? '' : $hangoutTimeValue,
      'file_error' => $fileError
    ));
  }
  else {
    outputError(0, $twig);
  }
?>
