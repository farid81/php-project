<?php
  // Start session for login
  session_start();

  require('vendor/autoload.php');

  // Initialize Meekro
  DB::$user = 'ipd';
  DB::$password = 'ipdipd';
  DB::$dbName = 'iroom';

  // Initialize Twig
  $loader = new \Twig\Loader\FilesystemLoader('templates');
  $twig = new \Twig\Environment($loader);


  // Is user logged in?
  $loggedIn = false;
  $currentUser = array();

  if (isset($_SESSION['ukey'])) {
    // Check if user's key exists in database
    $userQuery = DB::queryFirstRow('SELECT * FROM users WHERE session_key=%s', $_SESSION['ukey']);

    if ($userQuery != null) {
      $loggedIn = true;
      $currentUser = $userQuery;
    }
  }


  // Create 6-character token for user to give to other users so they can send a friend request
  function createFriendToken($length) {
    // Create a random 6-character string to be used for friend requests
    $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $output = '';

    for ($i = 0; $i < $length; $i++) {
      $output .= $chars[rand(0, strlen($chars) - 1)];
    }

    return $output;
  }


  // With apologies for bad the language! Clean up bad words in given string and return the cleaned string.
  // Using bad words list obtained from: https://www.cs.cmu.edu/~biglou/resources/bad-words.txt
  function cleanBadWords($input) {
    $badwords = explode(PHP_EOL, file_get_contents('includes/badwords.txt'));
    $goodwords = array('puppy', 'kitten', 'bunny', 'sunshine', 'balloon');

    // Add whitespace before and after the input so that bad words at the beginning and end of a sentence will be detected below
    $input = ' ' . $input . ' ';

    foreach ($badwords as $word) {
      // Find all instances of bad words surrounded by whitespace and replace.
      // Add whitespace/punctuation before and after word to avoid words such as 'Hello' (which contains 'hell') from being censored.
      $input = str_ireplace(array(
        ' ' . $word . ' ',
        ' ' . $word . '!',
        ' ' . $word . '.',
        ' ' . $word . '?',
        ' ' . $word . ',',
        ' ' . $word . ':',
        ' ' . $word . ';',
      ), ' ' . $goodwords[rand(0, count($goodwords) - 1)] . ' ', $input);
    }

    return trim($input);
  }


  // Output an error page to the user if something went wrong/isn't found
  function outputError($errorCode, $twig) {
    switch ($errorCode) {
      case 0:
        $messageText = 'Something went wrong :(';
        break;
      case 1:
        $messageText = 'Could not find that user.';
        break;
      default:
        $messageText = 'Something went wrong :(';
    }

    echo $twig->render('error.html', array(
      'message' => $messageText
    ));
  }


  // Truncate some text to a given length with an ellipses at the end
  function truncateText($text, $length) {
    if (strlen($text) > $length)
      return substr($text, 0, $length - 3) . '...';
    return $text;
  }


  // Perform all functions to clean and truncate text input
  function prepareInputString($text, $length) {
    return truncateText(cleanBadWords(filter_var($text, FILTER_SANITIZE_STRING)), $length);
  }
?>
