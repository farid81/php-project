<?php
  require_once('includes/functions.php');

  if ($loggedIn) {
    // Check if user is adding new achievement
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Ensure values are created, set to default if not
      if (!isset($_POST['title']) || strlen(trim($_POST['title'])) < 1) {
        $title = 'Untitled Achievement';
      }
      else {
        $title = prepareInputString($_POST['title'], 50);
      }

      if (!isset($_POST['description']) || strlen(trim($_POST['description'])) < 1) {
        $description = '';
      }
      else {
        $description = prepareInputString($_POST['description'], 200);
      }

      DB::insert('achievements', array(
        'user_name' => $currentUser['user_name'],
        'title' => $title,
        'description' => $description,
        'date_added' => time()
      ));

      header('Location: myroom.php?user=' . $currentUser['user_name'] . '#walloffame');
    }

    // Render page
    echo $twig->render('create-achievement.html', array(
      'loggedIn' => $loggedIn,
      'sessUserName' => $currentUser['user_name'],
      'sessDisplayName' => $loggedIn == true ? $currentUser['display_name'] : false
    ));
  }
  else {
    outputError(0, $twig);
  }
?>
