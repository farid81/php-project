// Prevent refresh from resubmitting form
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}

function showAllAchievements() {
  document.getElementById('oldAchievementsBox').style.display = 'block';
  document.getElementById('showAllAchButton').style.display = 'none';
}
