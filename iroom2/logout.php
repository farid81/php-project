<?php
  require_once('includes/functions.php');

  // Delete stored session key from database
  DB::update('users', array(
    'session_key' => NULL
  ), 'user_name=%s', $currentUser['user_name']);

  // Destroy session and redirect to index
  session_destroy();
  header('Location: index.php');
?>
