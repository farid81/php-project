<?php
  require_once('includes/functions.php');

  if (isset($_GET['user']) && $loggedIn) {
    // Get array of all users page user is friends with
    $isFriend = false;
    $allFriends = DB::query('SELECT friend_name FROM friendship WHERE user_name=%s', $_GET['user']);
    if (DB::count() > 0) {
      // Check whether the current user is friends with page user
      foreach ($allFriends as $friend) {
        if ($friend['friend_name'] == $currentUser['user_name']) {
          $isFriend = true;
        }
      }
    }

    // Check if user is viewing their own profile
    if (strtolower($_GET['user']) == $currentUser['user_name']) {
      renderMyRoom($twig, $currentUser, $currentUser, $loggedIn, true);
    }
    else if ($isFriend) {
      $pageUser = DB::queryFirstRow('SELECT * FROM users WHERE user_name=%s', strtolower($_GET['user']));
      if ($pageUser == null) {
        outputError(1, $twig);
      }
      else {
        renderMyRoom($twig, $pageUser, $currentUser, $loggedIn, false);
      }
    }
    else {
      outputError(1, $twig);
      die();
    }
  }
  else {
    outputError(1, $twig);
    die();
  }

  function renderMyRoom($twig, $pageUser, $currentUser, $loggedIn, $myPage) {
    // Handle myroom image
    $roomImage = $pageUser['room_image'] == null ? 'content/static/placeholder.png' : sprintf('content/user/%s/%s', $pageUser['user_name'], $pageUser['room_image']);


    // Create list of all shouts for display
    $allShouts = array();
    $shouts = DB::query('SELECT * FROM messages WHERE recipient=%s ORDER BY time_sent desc', $pageUser['user_name']);

    foreach ($shouts as $shout) {
      $senderInfo = DB::queryFirstRow('SELECT display_name FROM users WHERE user_name=%s', $shout['sender']);
      array_push($allShouts, array(
        'sender_user_name' => $shout['sender'],
        'sender_display_name' => $senderInfo['display_name'],
        'message' => htmlspecialchars_decode($shout['message']),
        'time_sent' => $shout['time_sent']
      ));
    }


    // Create list of all achievements for display
    $recentAchievements = array();
    $oldAchievements = array();
    $allAchievements = DB::query('SELECT * FROM achievements WHERE user_name=%s ORDER BY date_added desc', $pageUser['user_name']);

    // Check if there are more than three recent achievements, and split to recent and old arrays
    if (DB::count() > 3) {
      $recentAchievements = array_splice($allAchievements, 0, 3);
      $oldAchievements = $allAchievements;
    }
    else {
      $recentAchievements = $allAchievements;
    }


    echo $twig->render('myroom.html', array(
      'loggedIn' => $loggedIn,
      'is_my_page' => $myPage,
      'sessDisplayName' => $currentUser['display_name'],
      'sessUserName' => $currentUser['user_name'],
      'pageUserName' => $pageUser['user_name'],
      'item_display_name' => $pageUser['display_name'],
      'item_room_image' => $roomImage,
      'item_status' => $pageUser['status'],
      'item_email' => $pageUser['email'],
      'item_birthday' => $pageUser['birthday'],
      'item_food' => $pageUser['fave_food'],
      'item_movie' => $pageUser['fave_movie'],
      'item_game' => $pageUser['fave_game'],
      'item_hangout_time' => $pageUser['hangout_time'],
      'recent_achievements' => $recentAchievements,
      'old_achievements' => $oldAchievements,
      'shouts' => $allShouts
    ));
  }
?>
