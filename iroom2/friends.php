<?php
  require_once('includes/functions.php');

  if ($loggedIn) {
    // Create arrays to hold errors and status of a sent friend request
    $errors = array();
    $requestStatus = 0;

    // Check if user is sending a friend request
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Verify user name is entered correctly
      if (!isset($_POST['user_name']) || strlen(trim($_POST['user_name'])) < 1 || strlen(trim($_POST['user_name'])) > 30) {
        $errors['errUserName'] = true;
      }
      // Verify friend key is entered correctly
      if (!isset($_POST['friend_key']) || strlen(trim($_POST['friend_key'])) < 1 || strlen(trim($_POST['friend_key'])) > 30) {
        $errors['errFriendKey'] = true;
      }

      // Proceed if there are no errors
      if (count($errors) == 0) {
        // Clean input strings
        $userName = filter_var(trim($_POST['user_name']), FILTER_SANITIZE_STRING);
        $friendKey = filter_var(trim($_POST['friend_key']), FILTER_SANITIZE_STRING);

        // Check whether the recipient exists, and the friend key entered is correct
        $recipient = DB::queryFirstRow('SELECT user_name, friend_key FROM users WHERE user_name=%s', $userName);
        if ($recipient == null) {
          $requestStatus = 2;
        }
        else {
          if ($friendKey != $recipient['friend_key']) {
            $requestStatus = 2;
          }
          else {
            // Check if already friends
            $friendshipTest = DB::queryFirstRow('SELECT * FROM friendship WHERE user_name=%s AND friend_name=%s', $currentUser['user_name'], $recipient['user_name']);

            if ($friendshipTest != null) {
              $requestStatus = 2;
            }
            else {
              // If all is good, send request
              $requestStatus = 1;
              DB::insert('friend_requests', array(
                'sender' => $currentUser['user_name'],
                'recipient' => $recipient['user_name']
              ));
            }
          }
        }
      }
    }

    // Check if user has any outstanding friend requests and build array of them
    $pendingRequests = array();
    $requestCheck = DB::query('SELECT * FROM friend_requests WHERE recipient=%s', $currentUser['user_name']);

    if (DB::count() > 0) {
      foreach ($requestCheck as $req) {
        $senderDisplayName = DB::queryFirstRow('SELECT display_name FROM users WHERE user_name=%s', $req['sender']);

        array_push($pendingRequests, array(
          'sender_user_name' => $req['sender'],
          'sender_display_name' => $senderDisplayName['display_name']
        ));
      }
    }

    // Build array of user's friends to send to template
    $allFriends = DB::query('SELECT * FROM friendship WHERE user_name=%s', $currentUser['user_name']);
    $friendsInfo = array();

    if (DB::count() > 0) {
      foreach ($allFriends as $friend) {
        $info = DB::queryFirstRow('SELECT display_name, user_name, status FROM users WHERE user_name=%s ORDER BY user_name asc', $friend['friend_name']);

        if ($info != null) {
          array_push($friendsInfo, array(
            'user_name' => $info['user_name'],
            'display_name' => $info['display_name'],
            'status' => $info['status'],
            'friends_since' => $friend['friends_since']
          ));
        }
      }
    }


    echo $twig->render('friends.html', array_merge($errors, array(
      'loggedIn' => $loggedIn,
      'sessUserName' => $currentUser['user_name'],
      'sessDisplayName' => $currentUser['display_name'],
      'sessFriendKey' => $currentUser['friend_key'],
      'friends_list' => $friendsInfo,
      'requestStatus' => $requestStatus,
      'requests' => $pendingRequests
    )));
  }
  else {
    outputError(0, $twig);
  }
?>
