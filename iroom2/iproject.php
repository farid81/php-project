<?php
  require_once('includes/functions.php');

  if ($loggedIn) {
    echo $twig->render('iproject.html', array(
      'loggedIn' => $loggedIn,
      'sessUserName' => $currentUser['user_name'],
      'sessDisplayName' => $currentUser['display_name']
    ));
  }
  else {
    outputError(0, $twig);
  }
?>
