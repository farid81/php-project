<?php
  require_once('includes/functions.php');

  // Redirect user if already logged in
  if ($loggedIn) {
    header(sprintf('Location: myroom.php?user=%s', $currentUser['user_name']));
  }

  // Set variable to track whether an error occurs
  $error = false;

  // Check if user is logging in
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Check if any form values are missing
    if (
      !isset($_POST['userName'])
      || !isset($_POST['email'])
      || !isset($_POST['password'])
      || $_POST['userName'] == ''
      || $_POST['email'] == ''
      || $_POST['password'] == ''
    ) {
      $error = true;
    }
    else {
      // Try db query to see if user exists, create error if they don't
      $userInfo = DB::queryFirstRow('SELECT * FROM users WHERE user_name=%s', strtolower($_POST['userName']));
      if ($userInfo == null) {
        $error = true;
      }
      else {
        // Verify if email is correct
        $email = filter_var(trim($_POST['email']), FILTER_SANITIZE_EMAIL);
        if ($email != $userInfo['email']) {
          $error = true;
        }
        else {
          // Verify if passwords match
          if (!password_verify($_POST['password'], $userInfo['password'])) {
            $error = true;
          }
          else {
            // If all info matches, create a token to identify this session
            $token = bin2hex(openssl_random_pseudo_bytes(16));

            // Create session variable with key, and store key in database
            $_SESSION['ukey'] = $token;
            DB::update('users', array(
              'session_key' => $token
            ), 'user_name=%s', $userInfo['user_name']);

            // Redirect to user's page
            header(sprintf('Location: myroom.php?user=%s', $userInfo['user_name']));
          }
        }
      }
    }
  }


  // Render page
  echo $twig->render('login.html', array(
    'error' => $error,
    'loggedIn' => $loggedIn,
    'sessDisplayName' => $loggedIn == true ? $currentUser['display_name'] : false,
    'newAccount' => isset($_GET['newAccount']) ? true : false
  ));
?>
