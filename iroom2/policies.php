<?php
  require_once('includes/functions.php');

  // Render page
  if ($loggedIn) {
    echo $twig->render('policies.html', array(
      'loggedIn' => $loggedIn,
      'sessUserName' => $currentUser['user_name'],
      'sessDisplayName' => $loggedIn == true ? $currentUser['display_name'] : false
    ));
  }
  else {
    echo $twig->render('policies.html', array(
      'loggedIn' => $loggedIn
    ));
  }
?>
