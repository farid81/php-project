<?php
  require_once('includes/functions.php');

  if ($loggedIn) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $request = DB::queryFirstRow('SELECT * FROM friend_requests WHERE sender=%s AND recipient=%s', $_POST['requester'], $currentUser['user_name']);

      if ($request != null) {
        if (isset($_POST['accept'])) {
          // Request was accepted
          DB::insert('friendship', array(
            'user_name' => $currentUser['user_name'],
            'friend_name' => $_POST['requester'],
            'friends_since' => time()
          ));
          DB::insert('friendship', array(
            'user_name' => $_POST['requester'],
            'friend_name' => $currentUser['user_name'],
            'friends_since' => time()
          ));

          // Delete pending request
          DB::delete('friend_requests', 'id=%i', $request['id']);
        }
        else {
          // Request was rejected
          DB::delete('friend_requests', 'id=%i', $request['id']);
        }
      }
    }

    header('Location: friends.php');
  }
  else {
    outputError(0, $twig);
  }
?>
